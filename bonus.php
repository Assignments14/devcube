<?php

/*
The number 3797 has an interesting property . 
Being prime itself, it is possible to continuously remove digits from left to right, 
and remain prime at each stage: 3797, 797, 97, and 7. 
Similarly we can work from right to left: 3797, 379, 37, and 3.
Find the sum of the only eleven primes that are both truncatable from left to right and right to left .
NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes .
*/

function isPrime($num) {
    if ($num < 2) {
        return false;
    }
    for ($i = 2; $i <= sqrt($num); $i++) {
        if ($num % $i == 0) {
            return false;
        }
    }

    return true;
}

function isTruncatablePrime(int $num): bool {

    if (!isPrime($num)) {
        return false;
    }

    $numStr = (string)$num;
    $lenNumStr = strlen($num);

    // Check from left to right
    for ($i = 1; $i < $lenNumStr; $i++) {
        $leftSubstring = substr($numStr, $i);
        if (!isPrime((int)$leftSubstring)) {
            return false;
        }
    }

    // Check from right to left
    for ($i = 1; $i < $lenNumStr; $i++) {
        $rightSubstring = substr($numStr, 0, -$i);
        if (!isPrime((int)$rightSubstring)) {
            return false;
        }
    }

    return true;
}

$count = 0;
$sum = 0;
$num = 10; // Starting from 10, as 2, 3, 5, and 7 are not considered truncatable primes
$num_count = 11;

while ($count < $num_count) {
    if (isTruncatablePrime($num)) {
        $sum += $num;
        $count++;
    }
    $num++;
}

echo $sum .PHP_EOL;
