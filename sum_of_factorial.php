<?php

function factorial($n) {
    if ($n == 0 || $n == 1) {
        return '1';
    } else {
        return bcmul($n, factorial($n - 1), 0);
    }
}

function sumOfDigits($number) {
    $digitArray = str_split($number);
    return array_sum($digitArray);
}

$factorial100 = factorial(100);

$sumOfDigits = sumOfDigits($factorial100);

echo $sumOfDigits;

